(function($) {
    "use strict";
    $.fn.anarchytip = function(options) {
        var defaults = $.extend({
            xOffset: 30,
            yOffset: 10
        }, options);
        return this.each(function() {
            var a = $(this);
            var above;
            var animated;
            var edge;
            var $body = $("body");
            var $preview;

            function animationStop() {
                animated = false;
            }

            function animationStep(now, fx) {
                fx.end = edge;
            }

            function getAbove(e) {
                return e.clientY > $(window).height() / 2;
            }

            function calcEdge(e) {
                edge = above ? $(window).height() - e.clientY - defaults.yOffset : e.clientY - defaults.yOffset;
            }

            function setPos() {
                if (above) {
                    $preview.css({bottom: edge});
                } else {
                    $preview.css({top: edge});
                }
            }


            a.hover(function (e) {
                this.t = this.title;
                this.title = "";
                var c = (this.t != "") ? "<br/>" + this.t : "";
                $body.append("<p id='preview'><img src='" + a.data('full') + "' alt='Image preview' />" + c + "</p>");
                above = getAbove(e);
                calcEdge(e);
                $preview = $("#preview");
                setPos();
                $preview.css("left", e.clientX + defaults.xOffset).fadeIn();
            },
            function() {
                this.title = this.t;
                $("#preview").remove();
            });

            a.mousemove(function (e) {

                function prepareAnimation() {
                    if (above) {
                        $preview.css({top: 'auto', bottom: $preview.css('bottom')});
                        return {bottom: edge};
                    } else {
                        $preview.css({top: $preview.css('top'), bottom: 'auto'});
                        return {top: edge};
                    }
                }

                $preview.css("left", e.clientX + defaults.xOffset);
                var new_above = getAbove(e);
                var aboveChanged = new_above != above;
                above = new_above;
                calcEdge(e);
                if (animated) {
                    if (aboveChanged)
                        $preview.stop('x').animate(prepareAnimation(), {
                            complete: animationStop,
                            step: animationStep,
                            queue: 'x',
                        }).dequeue('x');
                }
                else
                    if (!aboveChanged)
                        setPos();
                    else {
                        animated = true;
                        $preview.animate(prepareAnimation(), {
                            complete: animationStop,
                            step: animationStep,
                            queue: 'x',
                        }).dequeue('x');
                    }


            })
        })
    }
})(jQuery);