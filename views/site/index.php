<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$pcont = '#pjax-container'; // const

$this->title = 'Реестр сотрудников';
$this->registerCssFile('@web/css/index.css', [
    'depends' => [app\assets\AppAsset::className()]
]);

$this->registerJs(/** @lang javascript */ "

$('.preview').anarchytip();
$('$pcont').on('pjax:success', function() {
    $('.preview').anarchytip();    
    
    var sort = window.location.search.match(/\bsort=(-?\w+)\b/i);
    if (sort) {
        sort = sort[1];
        var sf = $('#searchForm');
        var input = sf.children('input[type=\'hidden\'][name=\'sort\']');  

        if (input.length > 0)
            input.attr('value', sort);
        else {
            input = $('<input>', {'type': 'hidden', 'name': 'sort', 'value': sort});
            sf.prepend(input);            
        }                
    }
});

var currentRequests = {};
$('$pcont').ajaxSend(function(e, xhr, options) {
    if (options.url in currentRequests) {
        currentRequests[options.url]++;
        xhr.abort();
    } else 
        currentRequests[options.url] = 1;
});

$('$pcont').ajaxComplete(function(e, xhr, options) {
     if (currentRequests[options.url] == 1)
         delete currentRequests[options.url];
     else
         currentRequests[options.url]--;
});

");
?>


<?php

function renderSubmitButton()
{
    return Html::submitButton('Поиск', ['class' => 'btn btn-info']);
}

$form = ActiveForm::begin([
    'options' => [
        'class' => 'clearfix',
    ],
    'id' => 'searchForm',
    'method' => 'get',
    'action' => ['index', 'sort' => Yii::$app->request->get('sort')],
    'fieldConfig' => function ($model, $attribute) {
        $template = '{label}<div class="d1">{input}{error}' . '</div>' .
            ($attribute == 'maxAge' ? renderSubmitButton() : '');

        return compact('template');
    }
]); ?>

    <div class="text-search">
        <?= $form->field($sModel, 'name')->textInput() ?>
    </div>
    <div class="sex-search">
        <p>Пол</p>
        <?= $form->field($sModel, 'male')->checkbox() ?>
        <?= $form->field($sModel, 'female')->checkbox() ?>
    </div>
    <div class="age-search">
        <p>Возраст</p>
        <?= $form->field($sModel, 'minAge')->textInput() ?>
        <?= $form->field($sModel, 'maxAge')->textInput() ?>
    </div>
<?php ActiveForm::end(); ?>

<?= Html::a('+ Добавить сотрудника', Url::to(['create']), ['id' => 'link-create']) ?>



<?php Pjax::begin([
    'id' => substr($pcont, 1),
    'formSelector' => '#searchForm',
    'timeout' => 2000,
]); ?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}{pager}",
    'emptyText' => 'Ничего не найдено.',
    'columns' => [
        [
            'attribute' => 'id',
            'headerOptions' => ['style' => 'width:1px'],
        ],
        [
            'attribute' => 'photo',
            'headerOptions' => ['style' => 'width: 50px'],
            'contentOptions' => ['style' => 'text-align: center'],
            'format' => 'raw',
            'value' => function ($data) {
                $photo = $data->getPhoto();
                return Html::img($photo[0], isset($photo[1]) ? ['class' => 'preview', 'data-full' => $photo[1]] : []);
            }
        ],
        'name',
        [
            'attribute' => 'age',
            'headerOptions' => ['style' => 'width:10%'],
            'value' => function ($data) {
                return $data->age . ' лет';
            }
        ],
        [
            'attribute' => 'isMale',
            'headerOptions' => ['style' => 'width:10%'],
            'value' => function ($data) {
                return mb_substr($data::SEX[$data->isMale], 0, 3) . '.';
            },
            'contentOptions' => function ($data) {
                return ['style' => 'color: ' . ($data->isMale ? 'DodgerBlue' : '#ff4fdf')];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Действие',
            'headerOptions' => ['style' => 'width:10%'],
            'template' => '{update}, {delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('Ред', $url, [
                        'data-pjax' => 0,
                    ]);
                },
                'delete' => function($url, $model, $key) {
                    return Html::a('Удал', Url::to(['delete', 'id' => $model->id] + Yii::$app->request->get()), [
                        'data-pjax' => 1,
                        'data-confirm' => 'Вы уверены что хотите удалить сотрудника?',
                        'data-pjax-timeout' => 2000
                    ]);
                },
            ]
        ],
    ]
]) ?>
<?php Pjax::end(); ?>

