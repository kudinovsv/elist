<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->registerCss('

h1 {
    text-align: center;
    margin-bottom: 40px;
    font-size: 30px;
}
    
div.c1 {
    float: left;
    width: 140px;
    text-align: right;
    padding-top: 7px;
    padding-right: 10px;
    font-weight: bold;
}

div.c2 {
    margin-left: 140px;
    margin-right: 12px;
}

div.c3 {
    float: right;
    width: 10px;
    margin-top: -4px;
}

div.required div.c3:after {
    content: "*";
    color: red;        
}

.test-form {
    width: 800px;
    margin: 0 auto;
}

.form-group:nth-last-child(2) div.c1 {
    padding-top: 2px;
}

.btn.btn-danger {
    margin-left: 5px;
}

');

$this->registerJs('
 
var obj = $("#employee-birthday"), field = obj[0];    
obj.datepicker({
    beforeShow: function (input, inst) {
        setTimeout(function () {
            inst.dpDiv.css({
                top: field.offsetTop,
                left: field.offsetLeft + field.offsetWidth - $("#ui-datepicker-div")[0].offsetWidth,
            });
        }, 0);
    }
});
    
');

?>

<div class="test-form">

    <?php $form = ActiveForm::begin([
        'fieldConfig' => [
            'template' => '<div class="c1">{label}:</div><div class="c3"></div><div class="c2">{input}{error}</div>',
        ],
    ]); ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'birthday')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'dd.MM.yyyy',
        'language' => 'ru',
        'options' => ['class' => 'form-control', 'autocomplete' => 'off'],
    ]) ?>
    <?= $form->field($model, 'isMale')->dropDownList(['' => 'Выберите пол'] + $model::SEX,
        ['options' => ['' => ['disabled' => true, 'selected' => true]]]) ?>
    <?= $form->field($model, 'photoFile')->fileInput() ?>

    <div class="form-group">
        <div class="c2">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Отмена', Url::to(['index']), ['class' => 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
