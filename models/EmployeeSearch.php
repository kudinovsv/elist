<?php
namespace app\models;

use yii\base\Model;
use app\Utils\CustomCountDataProvider;
use yii\data\ActiveDataProvider;

class EmployeeSearch extends Model
{
    public $name, $male, $female, $minAge, $maxAge;

    public function rules()
    {
        return [
            [['name', 'male', 'female'], 'safe'],
            [['minAge', 'maxAge'], 'trim'],
            [['minAge', 'maxAge'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Поиск',
            'male' => 'Муж',
            'female' => 'Жен',
            'minAge' => 'c',
            'maxAge' => 'по',
        ];
    }

    const calc_field_sql = 'YEAR(CURDATE()) - YEAR(birthday) - IF(RIGHT(CURDATE(),5) < RIGHT(birthday,5),1,0)';

    private function configureQuery($query, $forCount) {
        $query->andFilterWhere(['like', 'name', $this->name]);
        if ($this->male != $this->female)
            $query->andFilterWhere(['isMale' => $this->male]);
        $condMethod = $forCount ? 'andFilterWhere' : 'andFilterHaving';
        if ($this->minAge !== '' && $this->maxAge !== '')
            $query->$condMethod(['between', $forCount ? self::calc_field_sql : 'age', $this->minAge, $this->maxAge]);
        else {
            $query->$condMethod(['>=', $forCount ? self::calc_field_sql : 'age', $this->minAge]);
            $query->$condMethod(['<=', $forCount ? self::calc_field_sql : 'age', $this->maxAge]);
        }
    }

    private function linkCheck(&$params) {
        unset($params['id']);
        $params[0] = 'site/index';
    }

    public function search($params)
    {

        $query = Employee::find()->select([
            '*',
            self::calc_field_sql .' AS age'
        ]);

        $dataProvider = new CustomCountDataProvider([
            'countCallback' => function ($db) {
                $q = Employee::find();
                if (!$this->getErrors())
                    $this->configureQuery($q, true);
                return $q->count('*', $db);
            },
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
                'class' => 'app\Utils\CustomPagination',
                'linkCheckCallback' => function (&$params) { $this->linkCheck($params); }
            ],
            'sort' => [
                'attributes' => ['id', 'name', 'age', 'isMale'],
                'defaultOrder' => ['id' => SORT_ASC],
                'class' => 'app\Utils\CustomSort',
                'linkCheckCallback' => function (&$params) { $this->linkCheck($params); }
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $this->configureQuery($query, false);

        return $dataProvider;
    }

    public function formName()
    {
        return '';
    }
}