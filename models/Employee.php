<?php

namespace app\models;


use Imagine\Image\Box;
use Yii;
use yii\db\ActiveRecord;
use yii\imagine\Image;

class Employee extends ActiveRecord
{
    public $photoFile, $firstName, $surname, $patronymic, $age;

    const SEX = [1 => 'Мужской', 0 => 'Женский'];

    public function rules()
    {
        return [
            [['isMale', 'birthday', 'firstName', 'surname'], 'required', 'message' => 'Поле не должно быть пустым.'],
            ['birthday', 'date',
                'format' => 'php:d.m.Y',
                'timestampAttribute' => 'birthday',
                'timestampAttributeFormat' => 'php:Y-m-d',
                'message' => 'Формат даты некорректен.'
            ],
            ['photoFile', 'file', 'extensions' => 'png, jpg'], //, 'maxSize' => 20],
            [['firstName', 'surname', 'patronymic'], 'string', 'max' => 30],
            [['firstName', 'surname', 'patronymic'], 'trim'],
            [['firstName', 'surname', 'patronymic'],
                function ($attr) {
                    if (strpos($this->{$attr}, ' ') !== false)
                        $this->addError($attr, 'Поле не должно содержать пробелов.');
                }
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => '№ id',
            'name' => 'ФИО',
            'firstName' => 'Имя',
            'age' => 'Возраст',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'birthday' => 'Дата рождения',
            'isMale' => 'Пол',
            'photoFile' => 'Фото',
            'photo' => 'Фото'
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        list($this->surname, $this->firstName, $this->patronymic) = explode(' ', $this->name);
    }

    private function getFolder($absolute = true)
    {
        return Yii::getAlias($absolute ? '@webroot' : '@web') . '/uploads/';
    }

    private function generateFilename()
    {
        return strtolower(md5(uniqid($this->photoFile->baseName, true)) . '.' . $this->photoFile->extension);
    }


    public function loadWithFile($data)
    {
        if (!$this->load($data))
            return false;

        $this->photoFile = \yii\web\UploadedFile::getInstance($this, 'photoFile');
        return true;
    }

    private function deletePhoto()
    {
        if (isset($this->photo)) {
            if (file_exists($this->getFolder() . $this->photo)) {
                unlink($this->getFolder() . $this->photo);
            }
            if (file_exists($this->getFolder() . $this->thumbnail)) {
                unlink($this->getFolder() . $this->thumbnail);
            }
        }
    }

    public function savePhotoAndThumbnail()
    {
        $this->photo = $this->generateFilename();
        $this->thumbnail = $this->generateFilename();
        $image = Image::getImagine()->open($this->photoFile->tempName);
        $image->thumbnail(new Box(75, 75))->
            save($this->getFolder() . $this->thumbnail, ['quality' => 90]);
        $originalSize = $image->getSize();
        if (max($originalSize->getWidth(), $originalSize->getHeight()) > 400)
            $image->thumbnail(new Box(400, 400))->
                save($this->getFolder() . $this->photo, ['quality' => 90]);
        else
            $this->photoFile->saveAs($this->getFolder() . $this->photo);
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (isset($this->photoFile)) {
            $this->deletePhoto();
            $this->savePhotoAndThumbnail();
        }

        $this->name = join(' ', [$this->surname, $this->firstName, $this->patronymic]);

        return true;
    }

    public function afterDelete()
    {
        $this->deletePhoto();
    }


    public function getPhoto() {
        return $this->photo ?
            [$this->getFolder(false) . $this->thumbnail, $this->getFolder(false) . $this->photo] :
            [Yii::getAlias('@web') . '/no-photo.jpg', null];

    }
}