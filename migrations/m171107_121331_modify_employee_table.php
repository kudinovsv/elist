<?php

use yii\db\Migration;

/**
 * Class m171107_121331_modify_employee_table
 */
class m171107_121331_modify_employee_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('employee', 'thumbnail', $this->string());
    }

    public function down()
    {
        $this->dropColumn('employee', 'thumbnail');
    }

}
