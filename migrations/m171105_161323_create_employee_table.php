<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee`.
 */
class m171105_161323_create_employee_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('employee', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'birthday' => $this->date(),
            'isMale' => $this->boolean(),
            'photo' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('employee');
    }
}
