<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 08.11.2017
 * Time: 21:20
 */

namespace app\Utils;


use yii\data\ActiveDataProvider;

class CustomCountDataProvider extends ActiveDataProvider
{
    public $countCallback;
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    protected function prepareTotalCount()
    {
        return ($this->countCallback)($this->db);
    }
}