<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 13.11.2017
 * Time: 12:41
 */

namespace app\Utils;

use Yii;
use yii\data\Sort;
use yii\web\Request;

class CustomSort extends Sort
{
    public $linkCheckCallback;

    public function createUrl($attribute, $absolute = false)
    {
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        $params[$this->sortParam] = $this->createSortParam($attribute);
        $params[0] = $this->route === null ? Yii::$app->controller->getRoute() : $this->route;
        $urlManager = $this->urlManager === null ? Yii::$app->getUrlManager() : $this->urlManager;
        ($this->linkCheckCallback)($params);
        if ($absolute) {
            return $urlManager->createAbsoluteUrl($params);
        }

        return $urlManager->createUrl($params);
    }

}