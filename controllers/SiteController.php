<?php

namespace app\controllers;

use Yii;
use app\models\Employee;
use app\models\EmployeeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreate()
    {
        $model = new Employee();

        if ($model->loadWithFile(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['index']);


        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->loadWithFile(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['index']);

        return $this->render('update', [
            'model' => $model
        ]);
    }

    private function renderData()
    {
        $sModel = new EmployeeSearch;
        $dataProvider = $sModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'sModel' => $sModel
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->renderData();
    }

    public function actionIndex()
    {
        return $this->renderData();
    }
}
